package fr.paladium.libs.discordnotifapi.api.dto;

import java.beans.ConstructorProperties;

public class DevTokenDTO {
   private String devToken;

   public static DevTokenDTO.DevTokenDTOBuilder builder() {
      return new DevTokenDTO.DevTokenDTOBuilder();
   }

   public String getDevToken() {
      return this.devToken;
   }

   public void setDevToken(String devToken) {
      this.devToken = devToken;
   }

   public boolean equals(Object o) {
      if (o == this) {
         return true;
      } else if (!(o instanceof DevTokenDTO)) {
         return false;
      } else {
         DevTokenDTO other = (DevTokenDTO)o;
         if (!other.canEqual(this)) {
            return false;
         } else {
            Object this$devToken = this.getDevToken();
            Object other$devToken = other.getDevToken();
            if (this$devToken == null) {
               if (other$devToken != null) {
                  return false;
               }
            } else if (!this$devToken.equals(other$devToken)) {
               return false;
            }

            return true;
         }
      }
   }

   protected boolean canEqual(Object other) {
      return other instanceof DevTokenDTO;
   }

   public int hashCode() {
      int result = 1;
      Object $devToken = this.getDevToken();
      result = result * 59 + ($devToken == null ? 0 : $devToken.hashCode());
      return result;
   }

   public String toString() {
      return "DevTokenDTO(devToken=" + this.getDevToken() + ")";
   }

   public DevTokenDTO() {
   }

   @ConstructorProperties({"devToken"})
   public DevTokenDTO(String devToken) {
      this.devToken = devToken;
   }

   public static class DevTokenDTOBuilder {
      private String devToken;

      DevTokenDTOBuilder() {
      }

      public DevTokenDTO.DevTokenDTOBuilder devToken(String devToken) {
         this.devToken = devToken;
         return this;
      }

      public DevTokenDTO build() {
         return new DevTokenDTO(this.devToken);
      }

      public String toString() {
         return "DevTokenDTO.DevTokenDTOBuilder(devToken=" + this.devToken + ")";
      }
   }
}
