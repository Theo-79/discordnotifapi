package fr.paladium.libs.discordnotifapi.api.dto;

import java.beans.ConstructorProperties;

public class TokenDTO {
   private String TokenType;
   private String Token;
   private String Timestamp;
   private String Status;

   public static TokenDTO.TokenDTOBuilder builder() {
      return new TokenDTO.TokenDTOBuilder();
   }

   public String getTokenType() {
      return this.TokenType;
   }

   public String getToken() {
      return this.Token;
   }

   public String getTimestamp() {
      return this.Timestamp;
   }

   public String getStatus() {
      return this.Status;
   }

   public void setTokenType(String TokenType) {
      this.TokenType = TokenType;
   }

   public void setToken(String Token) {
      this.Token = Token;
   }

   public void setTimestamp(String Timestamp) {
      this.Timestamp = Timestamp;
   }

   public void setStatus(String Status) {
      this.Status = Status;
   }

   public boolean equals(Object o) {
      if (o == this) {
         return true;
      } else if (!(o instanceof TokenDTO)) {
         return false;
      } else {
         TokenDTO other = (TokenDTO)o;
         if (!other.canEqual(this)) {
            return false;
         } else {
            label59: {
               Object this$TokenType = this.getTokenType();
               Object other$TokenType = other.getTokenType();
               if (this$TokenType == null) {
                  if (other$TokenType == null) {
                     break label59;
                  }
               } else if (this$TokenType.equals(other$TokenType)) {
                  break label59;
               }

               return false;
            }

            Object this$Token = this.getToken();
            Object other$Token = other.getToken();
            if (this$Token == null) {
               if (other$Token != null) {
                  return false;
               }
            } else if (!this$Token.equals(other$Token)) {
               return false;
            }

            Object this$Timestamp = this.getTimestamp();
            Object other$Timestamp = other.getTimestamp();
            if (this$Timestamp == null) {
               if (other$Timestamp != null) {
                  return false;
               }
            } else if (!this$Timestamp.equals(other$Timestamp)) {
               return false;
            }

            Object this$Status = this.getStatus();
            Object other$Status = other.getStatus();
            if (this$Status == null) {
               if (other$Status != null) {
                  return false;
               }
            } else if (!this$Status.equals(other$Status)) {
               return false;
            }

            return true;
         }
      }
   }

   protected boolean canEqual(Object other) {
      return other instanceof TokenDTO;
   }

   public int hashCode() {
      int result = 1;
      Object $TokenType = this.getTokenType();
      result = result * 59 + ($TokenType == null ? 0 : $TokenType.hashCode());
      Object $Token = this.getToken();
      result = result * 59 + ($Token == null ? 0 : $Token.hashCode());
      Object $Timestamp = this.getTimestamp();
      result = result * 59 + ($Timestamp == null ? 0 : $Timestamp.hashCode());
      Object $Status = this.getStatus();
      result = result * 59 + ($Status == null ? 0 : $Status.hashCode());
      return result;
   }

   public String toString() {
      return "TokenDTO(TokenType=" + this.getTokenType() + ", Token=" + this.getToken() + ", Timestamp=" + this.getTimestamp() + ", Status=" + this.getStatus() + ")";
   }

   public TokenDTO() {
   }

   @ConstructorProperties({"TokenType", "Token", "Timestamp", "Status"})
   public TokenDTO(String TokenType, String Token, String Timestamp, String Status) {
      this.TokenType = TokenType;
      this.Token = Token;
      this.Timestamp = Timestamp;
      this.Status = Status;
   }

   public static class TokenDTOBuilder {
      private String TokenType;
      private String Token;
      private String Timestamp;
      private String Status;

      TokenDTOBuilder() {
      }

      public TokenDTO.TokenDTOBuilder TokenType(String TokenType) {
         this.TokenType = TokenType;
         return this;
      }

      public TokenDTO.TokenDTOBuilder Token(String Token) {
         this.Token = Token;
         return this;
      }

      public TokenDTO.TokenDTOBuilder Timestamp(String Timestamp) {
         this.Timestamp = Timestamp;
         return this;
      }

      public TokenDTO.TokenDTOBuilder Status(String Status) {
         this.Status = Status;
         return this;
      }

      public TokenDTO build() {
         return new TokenDTO(this.TokenType, this.Token, this.Timestamp, this.Status);
      }

      public String toString() {
         return "TokenDTO.TokenDTOBuilder(TokenType=" + this.TokenType + ", Token=" + this.Token + ", Timestamp=" + this.Timestamp + ", Status=" + this.Status + ")";
      }
   }
}
