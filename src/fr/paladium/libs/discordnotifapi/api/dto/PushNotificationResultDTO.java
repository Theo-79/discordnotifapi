package fr.paladium.libs.discordnotifapi.api.dto;

import java.beans.ConstructorProperties;

public class PushNotificationResultDTO {
   private String TokenType;
   private String Token;
   private String Message;
   private String Timestamp;

   public static PushNotificationResultDTO.PushNotificationResultDTOBuilder builder() {
      return new PushNotificationResultDTO.PushNotificationResultDTOBuilder();
   }

   public String getTokenType() {
      return this.TokenType;
   }

   public String getToken() {
      return this.Token;
   }

   public String getMessage() {
      return this.Message;
   }

   public String getTimestamp() {
      return this.Timestamp;
   }

   public void setTokenType(String TokenType) {
      this.TokenType = TokenType;
   }

   public void setToken(String Token) {
      this.Token = Token;
   }

   public void setMessage(String Message) {
      this.Message = Message;
   }

   public void setTimestamp(String Timestamp) {
      this.Timestamp = Timestamp;
   }

   public boolean equals(Object o) {
      if (o == this) {
         return true;
      } else if (!(o instanceof PushNotificationResultDTO)) {
         return false;
      } else {
         PushNotificationResultDTO other = (PushNotificationResultDTO)o;
         if (!other.canEqual(this)) {
            return false;
         } else {
            label59: {
               Object this$TokenType = this.getTokenType();
               Object other$TokenType = other.getTokenType();
               if (this$TokenType == null) {
                  if (other$TokenType == null) {
                     break label59;
                  }
               } else if (this$TokenType.equals(other$TokenType)) {
                  break label59;
               }

               return false;
            }

            Object this$Token = this.getToken();
            Object other$Token = other.getToken();
            if (this$Token == null) {
               if (other$Token != null) {
                  return false;
               }
            } else if (!this$Token.equals(other$Token)) {
               return false;
            }

            Object this$Message = this.getMessage();
            Object other$Message = other.getMessage();
            if (this$Message == null) {
               if (other$Message != null) {
                  return false;
               }
            } else if (!this$Message.equals(other$Message)) {
               return false;
            }

            Object this$Timestamp = this.getTimestamp();
            Object other$Timestamp = other.getTimestamp();
            if (this$Timestamp == null) {
               if (other$Timestamp != null) {
                  return false;
               }
            } else if (!this$Timestamp.equals(other$Timestamp)) {
               return false;
            }

            return true;
         }
      }
   }

   protected boolean canEqual(Object other) {
      return other instanceof PushNotificationResultDTO;
   }

   public int hashCode() {
      int result = 1;
      Object $TokenType = this.getTokenType();
      result = result * 59 + ($TokenType == null ? 0 : $TokenType.hashCode());
      Object $Token = this.getToken();
      result = result * 59 + ($Token == null ? 0 : $Token.hashCode());
      Object $Message = this.getMessage();
      result = result * 59 + ($Message == null ? 0 : $Message.hashCode());
      Object $Timestamp = this.getTimestamp();
      result = result * 59 + ($Timestamp == null ? 0 : $Timestamp.hashCode());
      return result;
   }

   public String toString() {
      return "PushNotificationResultDTO(TokenType=" + this.getTokenType() + ", Token=" + this.getToken() + ", Message=" + this.getMessage() + ", Timestamp=" + this.getTimestamp() + ")";
   }

   public PushNotificationResultDTO() {
   }

   @ConstructorProperties({"TokenType", "Token", "Message", "Timestamp"})
   public PushNotificationResultDTO(String TokenType, String Token, String Message, String Timestamp) {
      this.TokenType = TokenType;
      this.Token = Token;
      this.Message = Message;
      this.Timestamp = Timestamp;
   }

   public static class PushNotificationResultDTOBuilder {
      private String TokenType;
      private String Token;
      private String Message;
      private String Timestamp;

      PushNotificationResultDTOBuilder() {
      }

      public PushNotificationResultDTO.PushNotificationResultDTOBuilder TokenType(String TokenType) {
         this.TokenType = TokenType;
         return this;
      }

      public PushNotificationResultDTO.PushNotificationResultDTOBuilder Token(String Token) {
         this.Token = Token;
         return this;
      }

      public PushNotificationResultDTO.PushNotificationResultDTOBuilder Message(String Message) {
         this.Message = Message;
         return this;
      }

      public PushNotificationResultDTO.PushNotificationResultDTOBuilder Timestamp(String Timestamp) {
         this.Timestamp = Timestamp;
         return this;
      }

      public PushNotificationResultDTO build() {
         return new PushNotificationResultDTO(this.TokenType, this.Token, this.Message, this.Timestamp);
      }

      public String toString() {
         return "PushNotificationResultDTO.PushNotificationResultDTOBuilder(TokenType=" + this.TokenType + ", Token=" + this.Token + ", Message=" + this.Message + ", Timestamp=" + this.Timestamp + ")";
      }
   }
}
