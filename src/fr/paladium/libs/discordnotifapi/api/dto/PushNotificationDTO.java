package fr.paladium.libs.discordnotifapi.api.dto;

import java.beans.ConstructorProperties;

public class PushNotificationDTO {
   private String token;
   private String message;

   public static PushNotificationDTO.PushNotificationDTOBuilder builder() {
      return new PushNotificationDTO.PushNotificationDTOBuilder();
   }

   public String getToken() {
      return this.token;
   }

   public String getMessage() {
      return this.message;
   }

   public void setToken(String token) {
      this.token = token;
   }

   public void setMessage(String message) {
      this.message = message;
   }

   public boolean equals(Object o) {
      if (o == this) {
         return true;
      } else if (!(o instanceof PushNotificationDTO)) {
         return false;
      } else {
         PushNotificationDTO other = (PushNotificationDTO)o;
         if (!other.canEqual(this)) {
            return false;
         } else {
            Object this$token = this.getToken();
            Object other$token = other.getToken();
            if (this$token == null) {
               if (other$token != null) {
                  return false;
               }
            } else if (!this$token.equals(other$token)) {
               return false;
            }

            Object this$message = this.getMessage();
            Object other$message = other.getMessage();
            if (this$message == null) {
               if (other$message != null) {
                  return false;
               }
            } else if (!this$message.equals(other$message)) {
               return false;
            }

            return true;
         }
      }
   }

   protected boolean canEqual(Object other) {
      return other instanceof PushNotificationDTO;
   }

   public int hashCode() {
      int result = 1;
      Object $token = this.getToken();
      int result1 = result * 59 + ($token == null ? 0 : $token.hashCode());
      Object $message = this.getMessage();
      result = result * 59 + ($message == null ? 0 : $message.hashCode());
      return result;
   }

   public String toString() {
      return "PushNotificationDTO(token=" + this.getToken() + ", message=" + this.getMessage() + ")";
   }

   public PushNotificationDTO() {
   }

   @ConstructorProperties({"token", "message"})
   public PushNotificationDTO(String token, String message) {
      this.token = token;
      this.message = message;
   }

   public static class PushNotificationDTOBuilder {
      private String token;
      private String message;

      PushNotificationDTOBuilder() {
      }

      public PushNotificationDTO.PushNotificationDTOBuilder token(String token) {
         this.token = token;
         return this;
      }

      public PushNotificationDTO.PushNotificationDTOBuilder message(String message) {
         this.message = message;
         return this;
      }

      public PushNotificationDTO build() {
         return new PushNotificationDTO(this.token, this.message);
      }

      public String toString() {
         return "PushNotificationDTO.PushNotificationDTOBuilder(token=" + this.token + ", message=" + this.message + ")";
      }
   }
}
