package fr.paladium.libs.discordnotifapi.api;

import fr.paladium.libs.discordnotifapi.api.dto.DevTokenDTO;
import fr.paladium.libs.discordnotifapi.api.dto.PushNotificationDTO;
import fr.paladium.libs.discordnotifapi.api.dto.PushNotificationResultDTO;
import fr.paladium.libs.discordnotifapi.api.dto.TokenDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MasterAPI {
   @POST("genToken")
   Call<TokenDTO> genToken(@Body DevTokenDTO var1);

   @POST("pushNotification")
   Call<PushNotificationResultDTO> pushNotification(@Body PushNotificationDTO var1);

   @DELETE("removeToken/{token}")
   Call<TokenDTO> deleteToken(@Path("token") String var1);
}
