package fr.paladium.libs.discordnotifapi.message;

public interface MessageBase {
   String getFinalMessage();
}
