package fr.paladium.libs.discordnotifapi.message;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class BasicDiscordMessage implements MessageBase {
   private String message;

   public BasicDiscordMessage(String message) {
      this.message = message;
   }

   public String getFinalMessage() {
      LinkedTreeMap<String, Object> json = new LinkedTreeMap();
      json.put("type", "message");
      json.put("message", this.message);
      Gson gson = new Gson();
      return gson.toJson((Object)json);
   }
}
