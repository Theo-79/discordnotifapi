package fr.paladium.libs.discordnotifapi.message;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class EmbedDiscordMessage implements MessageBase {
   private LinkedTreeMap<String, Object> json = new LinkedTreeMap();
   private LinkedTreeMap<String, Object[]> dataFields = new LinkedTreeMap();

   public EmbedDiscordMessage(String title) {
      this.json.put("title", title);
   }

   public void setTitle(String title) {
      if (this.json.containsKey("title")) {
         this.json.remove("title");
      }

      this.json.put("title", title);
   }

   public void addField(String name, String value, boolean inline) {
      this.dataFields.put(name, new Object[]{value, inline});
   }

   public void setColor(int color) {
      if (this.json.containsKey("color")) {
         this.json.remove("color");
      }

      this.json.put("color", color);
   }

   public void setAuthor(String name, String url, String iconUrl) {
      if (this.json.containsKey("author")) {
         this.json.remove("author");
      }

      this.json.put("author", new String[]{name, url, iconUrl});
   }

   public void setDescription(String description) {
      if (this.json.containsKey("description")) {
         this.json.remove("description");
      }

      this.json.put("description", description);
   }

   public void setImage(String url) {
      if (this.json.containsKey("image")) {
         this.json.remove("image");
      }

      this.json.put("image", url);
   }

   public void setThumbnail(String url) {
      if (this.json.containsKey("thumbnail")) {
         this.json.remove("thumbnail");
      }

      this.json.put("thumbnail", url);
   }

   public void setFooter(String text, String iconUrl) {
      if (this.json.containsKey("footer")) {
         this.json.remove("footer");
      }

      this.json.put("footer", new String[]{text, iconUrl});
   }

   public String getFinalMessage() {
      if (this.dataFields.size() != 0) {
         this.json.put("fields", this.dataFields);
      }

      this.json.put("type", "embed");
      Gson gson = new Gson();
      return gson.toJson((Object)this.json);
   }
}
