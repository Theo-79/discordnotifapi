package fr.paladium.libs.discordnotifapi;

import fr.paladium.libs.discordnotifapi.api.MasterAPI;
import fr.paladium.libs.discordnotifapi.api.dto.DevTokenDTO;
import fr.paladium.libs.discordnotifapi.api.dto.PushNotificationDTO;
import fr.paladium.libs.discordnotifapi.api.dto.PushNotificationResultDTO;
import fr.paladium.libs.discordnotifapi.api.dto.TokenDTO;
import fr.paladium.libs.discordnotifapi.exception.ApiReturnError;
import fr.paladium.libs.discordnotifapi.exception.ExceptionManager;
import fr.paladium.libs.discordnotifapi.message.MessageBase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import java.util.function.Consumer;

public class DiscordNotifAPI {
   private ExceptionManager exceptionManager = new ExceptionManager();
   private MasterAPI masterAPI_Discord;

   public DiscordNotifAPI(String endpointMaster) {
      this.masterAPI_Discord = (MasterAPI)(new Retrofit.Builder()).baseUrl(endpointMaster + "discord/").addConverterFactory(GsonConverterFactory.create()).build().create(MasterAPI.class);
   }

   private MasterAPI getMasterAPI(TokenType tokenType) {
      switch(tokenType) {
      case DISCORD:
         return this.masterAPI_Discord;
      default:
         return this.masterAPI_Discord;
      }
   }

   public String generateToken(String devToken, TokenType tokenType) throws Throwable {
      MasterAPI api = this.getMasterAPI(tokenType);
      DevTokenDTO devTokenDTO = DevTokenDTO.builder().devToken(devToken).build();
      Response<TokenDTO> response = api.genToken(devTokenDTO).execute();
      if (response != null && response.code() == 200) {
         if (response.body() == null) {
            throw new ApiReturnError("Api return null response on call master endpoint.");
         } else {
            return ((TokenDTO)response.body()).getToken();
         }
      } else if (response == null) {
         throw new ApiReturnError("Api return error on call master endpoint.");
      } else {
         throw this.exceptionManager.generateCustomException(response.code(), response.message());
      }
   }

   public void generateTokenAsync(String devToken, TokenType tokenType, final Consumer<String> success, final Consumer<Throwable> error) {
      MasterAPI api = this.getMasterAPI(tokenType);
      DevTokenDTO devTokenDTO = DevTokenDTO.builder().devToken(devToken).build();
      api.genToken(devTokenDTO).enqueue(new Callback<TokenDTO>() {
         public void onResponse(Call<TokenDTO> call, Response<TokenDTO> response) {
            if (response != null && response.code() == 200) {
               if (response.body() == null) {
                  error.accept(new ApiReturnError("Api return null response on call master endpoint."));
               } else {
                  success.accept(((TokenDTO)response.body()).getToken());
               }
            } else {
               if (response == null) {
                  error.accept(new ApiReturnError("Api return error on call master endpoint."));
               } else {
                  error.accept(DiscordNotifAPI.this.exceptionManager.generateCustomException(response.code(), response.message()));
               }

            }
         }

         public void onFailure(Call<TokenDTO> call, Throwable throwable) {
            error.accept(new ApiReturnError("Api return error on call master endpoint.", throwable));
         }
      });
   }

   public void pushNotification(TokenType tokenType, String token, MessageBase message) throws Throwable {
      MasterAPI api = this.getMasterAPI(tokenType);
      PushNotificationDTO pushNotificationDTO = PushNotificationDTO.builder().token(token).message(message.getFinalMessage()).build();
      Response<PushNotificationResultDTO> response = api.pushNotification(pushNotificationDTO).execute();
      if (response != null && response.code() == 200) {
         if (response.body() == null) {
            throw new ApiReturnError("Api return null response on call master endpoint.");
         }
      } else if (response == null) {
         throw new ApiReturnError("Api return error on call master endpoint.");
      } else {
         throw this.exceptionManager.generateCustomException(response.code(), response.message());
      }
   }

   public void pushNotificationAsync(TokenType tokenType, String token, MessageBase message, final Consumer<Void> success, final Consumer<Throwable> error) {
      MasterAPI api = this.getMasterAPI(tokenType);
      PushNotificationDTO pushNotificationDTO = PushNotificationDTO.builder().token(token).message(message.getFinalMessage()).build();
      api.pushNotification(pushNotificationDTO).enqueue(new Callback<PushNotificationResultDTO>() {
         public void onResponse(Call<PushNotificationResultDTO> call, Response<PushNotificationResultDTO> response) {
            if (response != null && response.code() == 200) {
               if (response.body() == null) {
                  error.accept(new ApiReturnError("Api return null response on call master endpoint."));
               } else {
                  success.accept((Void)null);
               }
            } else {
               if (response == null) {
                  error.accept(new ApiReturnError("Api return error on call master endpoint."));
               } else {
                  error.accept(DiscordNotifAPI.this.exceptionManager.generateCustomException(response.code(), response.message()));
               }

            }
         }

         public void onFailure(Call<PushNotificationResultDTO> call, Throwable throwable) {
            error.accept(new ApiReturnError("Api return error on call master endpoint.", throwable));
         }
      });
   }

   public void deleteToken(String token, TokenType tokenType) throws Throwable {
      MasterAPI api = this.getMasterAPI(tokenType);
      Response<TokenDTO> response = api.deleteToken(token).execute();
      if (response != null && response.code() == 200) {
         if (response.body() == null) {
            throw new ApiReturnError("Api return null response on call master endpoint.");
         }
      } else if (response == null) {
         throw new ApiReturnError("Api return error on call master endpoint.");
      } else {
         throw this.exceptionManager.generateCustomException(response.code(), response.message());
      }
   }

   public void deleteTokenAsync(String token, TokenType tokenType, final Consumer<Void> success, final Consumer<Throwable> error) {
      MasterAPI api = this.getMasterAPI(tokenType);
      api.deleteToken(token).enqueue(new Callback<TokenDTO>() {
         public void onResponse(Call<TokenDTO> call, Response<TokenDTO> response) {
            if (response != null && response.code() == 200) {
               if (response.body() == null) {
                  error.accept(new ApiReturnError("Api return null response on call master endpoint."));
               } else {
                  success.accept((Void)null);
               }
            } else {
               if (response == null) {
                  error.accept(new ApiReturnError("Api return error on call master endpoint."));
               } else {
                  error.accept(DiscordNotifAPI.this.exceptionManager.generateCustomException(response.code(), response.message()));
               }

            }
         }

         public void onFailure(Call<TokenDTO> call, Throwable throwable) {
            error.accept(new ApiReturnError("Api return error on call master endpoint.", throwable));
         }
      });
   }
}
