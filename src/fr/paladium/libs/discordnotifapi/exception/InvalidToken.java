package fr.paladium.libs.discordnotifapi.exception;

public class InvalidToken extends NotifBaseException {
   public InvalidToken() {
   }

   public InvalidToken(String s) {
      super(s);
   }

   public int getCode() {
      return 564;
   }
}
