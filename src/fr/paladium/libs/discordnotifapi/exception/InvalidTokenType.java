package fr.paladium.libs.discordnotifapi.exception;

public class InvalidTokenType extends NotifBaseException {
   public InvalidTokenType() {
   }

   public InvalidTokenType(String s) {
      super(s);
   }

   public int getCode() {
      return 563;
   }
}
