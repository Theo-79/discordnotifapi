package fr.paladium.libs.discordnotifapi.exception;

public class InvalidDevToken extends NotifBaseException {
   public InvalidDevToken() {
   }

   public InvalidDevToken(String s) {
      super(s);
   }

   public int getCode() {
      return 561;
   }
}
