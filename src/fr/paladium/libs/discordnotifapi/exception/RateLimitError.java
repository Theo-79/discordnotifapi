package fr.paladium.libs.discordnotifapi.exception;

public class RateLimitError extends NotifBaseException {
   public RateLimitError(String s) {
      super(s);
   }

   public int getCode() {
      return 566;
   }
}
