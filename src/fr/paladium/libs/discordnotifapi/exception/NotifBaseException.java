package fr.paladium.libs.discordnotifapi.exception;

public abstract class NotifBaseException extends Exception {
   public NotifBaseException() {
   }

   public NotifBaseException(String s) {
      super(s);
   }

   public NotifBaseException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public abstract int getCode();
}
