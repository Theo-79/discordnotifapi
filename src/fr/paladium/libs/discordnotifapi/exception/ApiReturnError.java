package fr.paladium.libs.discordnotifapi.exception;

public class ApiReturnError extends NotifBaseException {
   public ApiReturnError() {
   }

   public ApiReturnError(String s, Throwable throwable) {
      super(s, throwable);
   }

   public ApiReturnError(String s) {
      super(s);
   }

   public int getCode() {
      return 560;
   }
}
