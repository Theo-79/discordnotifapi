package fr.paladium.libs.discordnotifapi.exception;

public class RabbitMQReturnError extends NotifBaseException {
   public RabbitMQReturnError() {
   }

   public RabbitMQReturnError(String s, Throwable throwable) {
      super(s, throwable);
   }

   public int getCode() {
      return 565;
   }
}
