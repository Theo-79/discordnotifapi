package fr.paladium.libs.discordnotifapi.exception;

public class InvalidNotificationSender extends NotifBaseException {
   public InvalidNotificationSender() {
   }

   public InvalidNotificationSender(String s) {
      super(s);
   }

   public int getCode() {
      return 562;
   }
}
