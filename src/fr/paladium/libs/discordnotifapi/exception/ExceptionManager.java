package fr.paladium.libs.discordnotifapi.exception;

import java.util.HashMap;

public class ExceptionManager {
   private final HashMap<Integer, Class<? extends NotifBaseException>> EXCEPTION_LIST = new HashMap();

   public ExceptionManager() {
      this.registerExceptions();
   }

   public void registerExceptions() {
      this.registerException(ApiReturnError.class);
      this.registerException(InvalidDevToken.class);
      this.registerException(InvalidNotificationSender.class);
      this.registerException(InvalidToken.class);
      this.registerException(InvalidTokenType.class);
      this.registerException(RabbitMQReturnError.class);
      this.registerException(RateLimitError.class);
   }

   public void registerException(Class<? extends NotifBaseException> oClass) {
      try {
         NotifBaseException notifBaseException = (NotifBaseException)oClass.newInstance();
         this.EXCEPTION_LIST.put(notifBaseException.getCode(), oClass);
      } catch (InstantiationException var3) {
      } catch (IllegalAccessException var4) {
      }

   }

   private boolean codeExist(int code) {
      return this.EXCEPTION_LIST.containsKey(code);
   }

   public Throwable generateCustomException(int code, String message) {
      if (this.codeExist(code)) {
         try {
            return (Throwable)((Class)this.EXCEPTION_LIST.get(code)).getDeclaredConstructor(String.class).newInstance(message);
         } catch (Exception var4) {
            return new Exception(message);
         }
      } else {
         return new Exception(message);
      }
   }
}
